import maya.cmds as cmds
import import_export_skeleton.models as models
import io, json
import os

ROTATION_ORDERS = ["xyz", "yzx", "zxy", "xzy", "yxz", "zyx"]

# Functions Relating to Export

def export_skeleton(dir_name):
    selected = cmds.ls(sl=True, type="joint")
    if len(selected) == 1:
        curr_joint = selected
        
        curr_joint_as_obj = maya_joint_to_joint_obj(curr_joint)
        
        add_joint_children_to_obj(curr_joint, curr_joint_as_obj)

        if dir_name == None:
            dir_name = cmds.internalVar(usd=True)
        
        filename = os.path.join(dir_name, '{0}'.format((getSceneName() + "_skeleton_data.json")))
        
        with io.open(filename, 'w') as out_file:
            json.dump(curr_joint_as_obj, out_file, indent = 4,
                    ensure_ascii = False, default=lambda x: x.__dict__)
    else:
        cmds.error("Must select one joint to export skeleton")

    
def add_joint_children_to_obj(jnt, jnt_obj):
    joint_children = cmds.listRelatives(jnt, typ='transform')
    if joint_children == None or len(joint_children) == 0:
        return jnt_obj
    else:
        for child in joint_children:
            child_obj = maya_joint_to_joint_obj(child)
            jnt_obj.children.append(child_obj)
            add_joint_children_to_obj(child, child_obj)

def maya_joint_to_joint_obj(joint):
    selected = None
    if joint:
        selected = cmds.ls(joint)
    else:
        selected = cmds.ls(sl=True, type="joint")
        
    if len(selected) == 1:
        jnt = selected[0]
        jnt_rotation_order = ROTATION_ORDERS[cmds.getAttr('{0}.rotateOrder'.format(jnt))]
        jnt_pos = cmds.xform(jnt, query=True, worldSpace=True, rotatePivot=True)
        jnt_rot = cmds.xform(jnt, query=True, worldSpace=True, rotation=True)
        new_joint_obj = models.JointObject(jnt, jnt_pos, jnt_rot, jnt_rotation_order, [])
        return new_joint_obj
    else:
        cmds.error('Only one joint at a time can be parsed')
        
# Functions Relating to Import
def import_skeleton(file_path):
    if file_path == None or file_path == "":
        cmds.error('Empty file path')
    else:
        loaded_skeleton = None
        with open(file_path, 'r') as skeleton:
            loaded_skeleton = json.loads(skeleton.read(), object_hook=lambda d: models.JointObject(**d))
        
        if loaded_skeleton:
            curr_joint_obj = loaded_skeleton
            obj_as_jnt = maya_joint_obj_to_joint(curr_joint_obj)
            traverse_jnt_obj(obj_as_jnt, curr_joint_obj)

def maya_joint_obj_to_joint(joint_obj):
    cmds.select( clear=True )
    existing_jnt_with_name = cmds.ls(joint_obj.name)
    if existing_jnt_with_name:
        joint_obj.name = joint_obj.name + '_copy'
    new_joint = cmds.joint(n=joint_obj.name, p=joint_obj.pos, roo=joint_obj.rotation_order, o=joint_obj.rot)
    return new_joint
        
        
def traverse_jnt_obj(jnt, jnt_obj):
    if jnt_obj.children == None or len(jnt_obj.children) == 0:
        return jnt
    else:
        for child_obj in jnt_obj.children:
            child_jnt = maya_joint_obj_to_joint(child_obj)
            cmds.parent(child_jnt, jnt)
            traverse_jnt_obj(child_jnt, child_obj)
    

    
def getSceneName():
    filepath = cmds.file(q=True, sn=True)
    filename = os.path.basename(filepath)
    raw_name, extension = os.path.splitext(filename)
    return raw_name
    
if __name__ == '__main__':
    # export_skeleton(None)
    import_skeleton('C:/Users/kogan/OneDrive/Documents/maya/2022/scripts/import_export_skeleton/data/test_skeleton_data.json')