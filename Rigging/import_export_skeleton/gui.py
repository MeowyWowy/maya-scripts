from maya import cmds
from maya import mel
from maya import OpenMayaUI as omui 

from PySide2.QtCore import * 
from PySide2.QtGui import * 
from PySide2.QtWidgets import *
from shiboken2 import wrapInstance
from import_export_skeleton.functions import export_skeleton, import_skeleton 

mayaMainWindowPtr = omui.MQtUtil.mainWindow() 
mayaMainWindow = wrapInstance(int(mayaMainWindowPtr), QWidget) 

class CreateImportExportSkeletonUi(QWidget):
    def __init__(self, *args, **kwargs):
        super(CreateImportExportSkeletonUi, self).__init__(*args, **kwargs)
        self.setParent(mayaMainWindow)
        self.setWindowFlags(Qt.Window)
        self.setObjectName('Import_Export_Skeleton_uniqueId')
        self.setWindowTitle('Import/Export Skeleton JSON')
        self.setGeometry(50, 50, 500, 150)
        self.default_file_path = cmds.internalVar(usd=True) + '/import_export_skeleton/data'
        self.initUI()
    
    def export_skeleton_button_onClicked(self):
        export_skeleton(self.export_file_input.text())
    
    def export_path_button_onClicked(self):
        filepath = QFileDialog.getExistingDirectory(parent=self, caption='Select output file', dir=self.default_file_path)
        if filepath:
            self.export_file_input.setText(filepath)
            
    def import_path_button_onClicked(self):
        filename, filter = QFileDialog.getOpenFileNames(parent=self, caption='Select import file', dir=self.default_file_path, filter='JSON (*.json)')
        if filename:
            self.import_file_input.setText(filename[0])

    def import_skeleton_button_onClicked(self):
        import_skeleton(self.import_file_input.text())
            
    def cancel_button_onClicked(self):
        self.close()
        
# create UI    
    def initUI(self):
        layout = QGridLayout(self)
        export_file_label = QLabel(self)
        export_file_label.setText("Export File To:")
        self.export_file_input = QLineEdit(self)
        self.export_file_input.setText(self.default_file_path)
        self.export_path_button = QPushButton('...', self)

        
        import_file_label = QLabel(self)
        import_file_label.setText("Import File Location:")
        self.import_file_input = QLineEdit('')
        self.import_path_button = QPushButton('...', self)
        
        self.export_skeleton_button = QPushButton('Export Skeleton', self)
        self.import_skeleton_button = QPushButton('Import Skeleton', self)
        self.cancel_button = QPushButton('Close', self)
        
        layout.addWidget(export_file_label, 1, 1)
        layout.addWidget(self.export_file_input, 1, 2, 1, 4)
        layout.addWidget(self.export_path_button, 1, 6)
        
        layout.addWidget(self.export_skeleton_button, 4,2)
        
        layout.addWidget(import_file_label, 3, 1)
        layout.addWidget(self.import_file_input, 3, 2, 1, 4)
        layout.addWidget(self.import_path_button, 3, 6)
        
        layout.addWidget(self.import_skeleton_button, 4,3)
        
        layout.addWidget(self.cancel_button,4,4)


        self.export_skeleton_button.clicked.connect(self.export_skeleton_button_onClicked)
        self.cancel_button.clicked.connect(self.cancel_button_onClicked)
        self.import_skeleton_button.clicked.connect(self.import_skeleton_button_onClicked)
        self.export_path_button.clicked.connect(self.export_path_button_onClicked)
        self.import_path_button.clicked.connect(self.import_path_button_onClicked)
    
def main():
    ui = CreateImportExportSkeletonUi()
    ui.show()
    return ui
    
if __name__ == '__main__':
    main()