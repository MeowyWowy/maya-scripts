import maya.cmds

class JointObject:
    def __init__(self, name, pos, rot, rotation_order, children = []):
        self.name = name
        self.pos = pos
        self.rot = rot
        self.rotation_order = rotation_order
        self.children = children
