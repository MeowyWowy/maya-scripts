import maya.cmds as cmds
from global_maya_scripts import utils

def auto_ik_spline():
    selected = cmds.ls(sl=True)
    if selected and len(selected == 2):
        if utils.get_object_type(selected[0]) == 'joint' and utils.get_object_type(selected[1]) == 'joint':
            root_joint = selected[0]
            end_joint = selected[1]
            ik_handle = cmds.ikHandle(sj=root_joint, ee=end_joint, sol='ikSplineSolver')
            curve = cmds.ls(ik_handle[2])[0]
            curve_cvs = cmds.ls("{0}.cv[:]".format(curve), fl=True)
            cluster_handles = utils.create_clusters_on_selected(curve_cvs)
            for handle in cluster_handles:
                location = cmds.xform(handle, query=True, worldSpace=True, rotatePivot=True)
                cmds.circle(center=location)
        else:
            cmds.error('Selected objects must be joints!')
    else:
        cmds.error('Must select 2 joints!')

auto_ik_spline()