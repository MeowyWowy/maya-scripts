import maya.cmds as cmds

def get_object_type(obj):
    shapeNode = cmds.listRelatives(obj, shapes=True)
    nodeType = cmds.nodeType(shapeNode)
    return nodeType;
    
def create_clusters_on_selected(sel=cmds.ls(sl=True)):
    return_list = []
    for item in sel:
        curr_cluster = cmds.cluster(item)
        return_list.append(curr_cluster[1])
    return return_list