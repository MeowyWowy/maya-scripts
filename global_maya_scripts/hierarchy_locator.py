import maya.cmds as cmds
from global_maya_scripts import utils

def create_hierarchy_locator():
    pressPosition = cmds.draggerContext('hierarchyLocatorContext', query=True, anchorPoint=True)
    selected = cmds.ls(sl=True)
    if selected and len(selected) == 1 and utils.get_object_type(selected[0]) == 'locator':
        newLocator = cmds.spaceLocator()
        cmds.xform(newLocator, t=pressPosition)
        cmds.parent(newLocator, selected)
    else:
        newLocator = cmds.spaceLocator()
        cmds.xform(newLocator, t=pressPosition)
    
    
if cmds.draggerContext('hierarchyLocatorContext', exists=True):
    cmds.deleteUI('hierarchyLocatorContext')

cmds.draggerContext( 'hierarchyLocatorContext', pressCommand='create_hierarchy_locator()', cursor='hand',
pr='objectViewPlane', sp='world')

cmds.setToolTo('hierarchyLocatorContext')

