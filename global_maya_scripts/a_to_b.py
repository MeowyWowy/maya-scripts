import maya.cmds as cmds
def a_to_b(sel=[]):
    if len(sel) <= 0:
        sel = cmds.ls(selection=True)
    if len(sel) != 2:
        cmds.error("Requires two arguments")
    a = sel[0]
    b = sel[1]
    
    b_pos = cmds.xform(sel[1], query=True, worldSpace=True, rotatePivot=True)
    b_rot = cmds.xform(sel[1], query=True, worldSpace=True, rotation=True)
    cmds.xform(a, t=b_pos, worldSpace=True)
    cmds.xform(a, r=True, ro=b_rot, worldSpace=True)