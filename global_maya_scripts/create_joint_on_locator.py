import maya.cmds as cmds
from global_maya_scripts import utils


def create_joints_on_locator_chain():
    selected = cmds.ls(sl=True)
    root_locator = selected[0]
    curr_locator = root_locator
    if len(selected) == 1 and utils.get_object_type(root_locator) == 'locator':
        locator_children = cmds.listRelatives(curr_locator, typ='transform')
        last_joint_in_chain = None
        root_joint = create_joint_on_locator(curr_locator)
        last_joint_in_chain = root_joint
        while (locator_children is not None):
            curr_locator = locator_children[0]
            last_joint_in_chain = create_joint_on_locator(curr_locator, last_joint_in_chain)
            locator_children = cmds.listRelatives(curr_locator, typ='transform')
        cmds.delete(root_locator)
        return (root_joint, last_joint_in_chain)
    else:
        cmds.error('Must select a single locator')
        
def create_joint_on_locator(locator, parent_joint = None):
    joint = cmds.joint()
    point_constraint = cmds.pointConstraint(locator, joint)
    cmds.delete(point_constraint)
    cmds.parent(joint, world=True)
    if (parent_joint is not None):
        cmds.parent(joint, parent_joint)
    return joint
    
create_joints_on_locator_chain()