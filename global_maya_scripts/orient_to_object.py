import maya.cmds as cmds
import a_to_b

def reset_to_origin(node, node_pos=False):
    if not node_pos:
        node_pos = cmds.xform(node, query=True, worldSpace=True, rotatePivot=True)
    cmds.makeIdentity(node, apply=True, translate=True, rotate=False, scale=False, normal=False)
    
    node_offset = [p * -1 for p in node_pos]
    cmds.xform(node, worldSpace=True, translation=node_offset)
    
    cmds.setAttr(node + ".rotate", 0, 0, 0)
    cmds.makeIdentity(node, apply=True, translate=True, rotate=True, scale=False, normal=False)
    



def align_lras(snap_align=False,delete_history=True):
    sel = cmds.ls(sl=True)
    if len(sel) != 2:
        cmds.error("You must select a controller and a joint")
    ctrl = sel[0]
    jnt = sel[1]
    
    #check for parent node
    parent_node = cmds.listRelatives(ctrl, parent=True)
    if parent_node:
        cmds.parent(ctrl, world=True)

    jnt_matrix = cmds.xform(jnt, query=True, worldSpace=True, matrix=True)
    jnt_pos = cmds.xform(jnt, query=True, worldSpace=True, rotatePivot=True)
    jnt_rot = cmds.xform(jnt, query=True, worldSpace=True, rotation=True)
    ctrl_pos = cmds.xform(ctrl, query=True, worldSpace=True, rotatePivot=True)
    ctrl_rot = cmds.xform(ctrl, query=True, worldSpace=True, rotation=True)
    

    if cmds.objExists(ctrl + ".offsetParentMatrix"):
        off_grp = False
        cmds.setAttr(ctrl + ".offsetParentMatrix", 
        [1.0, 0.0, 0.0, 0.0, 
        0.0, 1.0, 0.0, 0.0, 0.0, 
        0.0, 1.0, 0.0, 0.0, 
        0.0, 0.0, 1.0], type="matrix")
        reset_to_origin(ctrl)
        cmds.setAttr(ctrl + ".offsetParentMatrix", jnt_matrix, type="matrix")
        
        if parent_node:
            #make temporary joint to help calculate offset matrix
            temp_parent_joint = cmds.joint(None, name='temp_parent_joint')
            temp_child_joint = cmds.joint(temp_parent_joint, name='temp_child_joint')
            a_to_b(sel=[temp_parent_joint, parent_node[0]])
            a_to_b(sel=[temp_child_joint, jnt])
            cmds.parent(ctrl, parent_node[0])
            reset_transforms(ctrl)
       
            child_matrix = cmds.getAttr(temp_child_joint + ".matrix")
            cmds.setAttr(ctrl + ".offsetParentMatrix", child_matrix, type="matrix")
            cmds.delete(temp_parent_joint)

