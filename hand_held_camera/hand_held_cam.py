from maya import cmds
from maya import mel
from maya import OpenMayaUI as omui 

from PySide2.QtCore import * 
from PySide2.QtGui import * 
from PySide2.QtWidgets import *
from shiboken2 import wrapInstance 
import random

mayaMainWindowPtr = omui.MQtUtil.mainWindow() 
mayaMainWindow = wrapInstance(int(mayaMainWindowPtr), QWidget) 

class CreateHandheldCamUi(QWidget):
    def __init__(self, *args, **kwargs):
        super(CreateHandheldCamUi, self).__init__(*args, **kwargs)
        self.setParent(mayaMainWindow)
        self.setWindowFlags(Qt.Window)
        self.setObjectName('CreateHandheldCamUi_uniqueId')
        self.setWindowTitle('Key Handheld Cam')
        self.setGeometry(50, 50, 350, 150)
        self.initUI()
        self.cmd = ''
        self.start_frame_val = None
        self.end_frame_val = None
        self.shake = None
        self.camera = None
        self.focus = None
        self.aim_constraint = None
        self.camera_linked_locator = None
        self.camera_ctrl = None
        self.anim_layer = None
        self.aim_constraint_last_weight_value_on_focus = None
        
    def ok_button_onClicked(self):
        selected = cmds.ls(sl=True)
        if len(selected) == 2:
            self.camera = selected[0]
            self.focus = selected[1]
            camera_shape_node = cmds.listRelatives(cmds.ls(sl=True)[0], shapes=True)
            if camera_shape_node[0] in cmds.listCameras() or self.camera in cmds.listCameras():
                self.setUpAimConstraintRig()
                
                cmds.select(self.camera_ctrl)
                self.anim_layer = cmds.animLayer('handheld_cam_layer', addSelectedObjects=True)
                
                self.start_frame_val = self.frame_range_low.value()
                self.end_frame_val = self.frame_range_high.value()
                self.shake = abs(self.shake_slider.value())
                curr_frame = self.start_frame_val
                return_coordinates = cmds.xform(self.camera_ctrl, query=True, t=True, ws=True)
                self.placeKeys(1, True, return_coordinates[0], return_coordinates[1], return_coordinates[2])
                last_coordinates = cmds.xform(self.camera_ctrl, query=True, t=True, ws=True)
                self.has_camera_person_noticed_shift = False
                number_keys_since_noticed = 0
                last_frame_noticed = 0
                while curr_frame < self.end_frame_val:
                    curr_frame = self.calculateNextFrame(curr_frame)
                    has_camera_person_noticed_shift = self.doesCameraPersonNotice(number_keys_since_noticed)
                    if has_camera_person_noticed_shift:
                        last_frame_noticed = curr_frame
                    number_keys_since_noticed += curr_frame - last_frame_noticed
                    x_translate = None
                    y_translate = None
                    z_translate = None
                    if has_camera_person_noticed_shift:
                        x_translate = self.calculateMovement(return_coordinates[0], 'x')
                        y_translate = self.calculateMovement(return_coordinates[1], 'y')
                        z_translate = self.calculateMovement(return_coordinates[2], 'z')
                        number_keys_since_noticed = 0
                    else:
                        x_translate = self.calculateMovement(last_coordinates[0], 'x')
                        y_translate = self.calculateMovement(last_coordinates[1], 'y')
                        z_translate = self.calculateMovement(last_coordinates[2], 'z')
                    self.placeKeys(curr_frame, 
                    self.doesKeyAimConstraint(has_camera_person_noticed_shift), 
                    x_translate, y_translate, z_translate)
                    last_coordinates = cmds.xform(self.camera_ctrl, query=True, t=True, ws=True)
                
            else:
                cmds.error('First selection must be a camera')
        else:
            cmds.error('Must select a camera and a focal object')

    def setUpAimConstraintRig(self):
        self.camera_ctrl = cmds.circle(center=(0, 0, 0), name='camera_CTRL', normal=(0, 1, 0), 
        sweep=360, radius=1, degree=3, useTolerance=False, constructionHistory=True)[0]
        cmds.parent('{0}'.format(self.camera), '{0}'.format(self.camera_ctrl))
        cmds.spaceLocator(name='camera_track_LOC')
        self.camera_linked_locator = cmds.ls("camera_track_LOC")[0]
        cmds.connectAttr('{0}.translateX'.format(self.camera), '{0}.translateX'.format(self.camera_linked_locator))
        cmds.connectAttr('{0}.translateY'.format(self.camera), '{0}.translateY'.format(self.camera_linked_locator))
        self.aim_constraint = cmds.aimConstraint([self.focus, self.camera_linked_locator], self.camera, maintainOffset = True)
        self.aim_constraint_last_weight_value_on_focus = cmds.getAttr('{1}.{0}W0'.format(self.focus, self.aim_constraint[0]))
    
    def calculateNextFrame(self, curr_frame):
        return curr_frame + self.shake + random.randrange(1,2)
    
    def placeKeys(self, time, does_key_aim_constraint, x, y, z):
        cmds.setKeyframe(self.camera_ctrl, attribute='translateX', t=time, value=x, animLayer = self.anim_layer)
        cmds.setKeyframe(self.camera_ctrl, attribute='translateY', t=time, value=y, animLayer = self.anim_layer)
        cmds.setKeyframe(self.camera_ctrl, attribute='translateZ', t=time, value=z, animLayer = self.anim_layer)
        if does_key_aim_constraint:
            aim_constraint_set_value = 0 if self.aim_constraint_last_weight_value_on_focus == 1 else 1
            self.aim_constraint_last_weight_value_on_focus = aim_constraint_set_value
            cmds.setKeyframe(self.aim_constraint, attribute='{0}W0'.format(self.focus), 
            t=time, value=aim_constraint_set_value)
            cmds.setKeyframe(self.aim_constraint, attribute='{0}W1'.format(self.camera_linked_locator), 
            t=time, value= 0 if aim_constraint_set_value == 1 else 1)

        
    def calculateMovement(self, curr_position, axis):
        shake_factor = random.uniform(-.05,.05)
        if (axis == 'x'):
            if random.randrange(1, 10) >= 7:
                return curr_position - shake_factor
            else:
                return curr_position + shake_factor
        elif (axis == 'y'):
            if random.randrange(1, 10) >= 7:
                return curr_position + shake_factor
            else:
                return curr_position - shake_factor
        else:
            if random.randrange(1, 10) % 2 == 0:
                return curr_position + shake_factor
            else:
                return curr_position - shake_factor
    
    def doesKeyAimConstraint(self, has_camera_person_noticed_shift):
        if has_camera_person_noticed_shift:
            return True
        else:
            return False

    def doesCameraPersonNotice(self, keys_since_last_noticed):
        percent_chance_to_notice = (keys_since_last_noticed / round(self.end_frame_val / 4)) * 100
        return True if random.randrange(1,100) < percent_chance_to_notice else False


    def clear_animation_button_onClicked(self):
        cmds.delete(self.anim_layer)
        cmds.delete(self.aim_constraint)
        cmds.parent('{0}'.format(self.camera), world=True)
        cmds.delete(self.camera_ctrl)
        cmds.delete(self.camera_linked_locator)
    
    def cancel_button_onClicked(self):
        self.close()
    
    # create UI    
    def initUI(self):
        label_frame_range = QLabel(self)
        label_frame_range.setText("Frame Range:")
        self.frame_range_low = QSpinBox(self)
        self.frame_range_low.setRange(-10000, 10000)
        self.frame_range_low.setBaseSize(40, 10)
        self.frame_range_low.setValue(cmds.playbackOptions( q=True,min=True ))
        self.frame_range_high = QSpinBox(self)
        self.frame_range_high.setRange(-10000, 10000)
        self.frame_range_high.setBaseSize(40, 10)
        self.frame_range_high.setValue(cmds.playbackOptions(q=True,max=True))
        label_frame_range.setBuddy(self.frame_range_low)
        label_frame_range.setBuddy(self.frame_range_high)
        
        label_shake = QLabel(self)
        label_shake.setText("Shake:")
        self.shake_slider = QSlider(Qt.Horizontal)
        self.shake_slider.setTickInterval(1)
        self.shake_slider.setMinimum(-10)
        self.shake_slider.setMaximum(-1)
        self.shake_slider.setValue(-10)
        
        layout = QGridLayout(self)
        layout.addWidget(label_frame_range, 0,0)
        layout.addWidget(self.frame_range_low, 0,1,1,2)
        layout.addWidget(self.frame_range_high, 0,3,1,2)
        layout.addWidget(label_shake, 1, 0,1,1)
        layout.addWidget(self.shake_slider, 1, 1, 1, 4)

        self.ok_button = QPushButton('Key to Animation Layer', self)
        self.clear_animation = QPushButton('Clear Animation', self)
        self.cancel_button = QPushButton('Close', self)
        layout.addWidget(self.ok_button, 2,2)
        layout.addWidget(self.cancel_button,2,4)
        layout.addWidget(self.clear_animation, 2,3)
        self.ok_button.clicked.connect(self.ok_button_onClicked)
        self.cancel_button.clicked.connect(self.cancel_button_onClicked)
        self.clear_animation.clicked.connect(self.clear_animation_button_onClicked)
            
def main():
    ui = CreateHandheldCamUi()
    ui.show()
    return ui
    
if __name__ == '__main__':
    main()
